
DESCRIPTION
===========

This is a simple module that moves all JavaScript tags to the bottom of every page.
Placing script tags at the bottom of a page, so that their loading and execution 
doesn't block rendering of the page content, is a well-established best practice. 
JavaScript in Drupal places tags in the header by default. JS-to-Bottom moves these 
tags to the bottom so that your pages will load faster and
your visitors will be happier.

There are, however, some cases in which you'll want certain scripts to load in the
header or somewhere other than the bottom of the page. In such a case, just specify
the script's path in JS-to-Bottom's configuration page so that it will
leave that script alone.

INSTALLATION
============

Nothing special. See:
http://drupal.org/documentation/install/modules-themes/modules-7

CONFIGURATION
=============

Settings for this module are at:
admin/config/development/js-to-bottom

CONTACT
=======

Current maintainers:
  Will Hastings (http://drupal.org/user/2368318)
