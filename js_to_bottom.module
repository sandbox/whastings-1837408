<?php

/**
 * @file
 * Defines JS-to-Bottom's functions and hook implementations.
 */

/**
 * Helpful function to move all script tags to page bottom
 *
 * @param $js
 *   Associative array of JavaScript resources to modify
 * @param $excludes
 *   List of scripts (specified by path) to leave alone
 */
function js_to_bottom_move_scripts(&$js, $excludes = array()) {
  //Loop through each JavaScript source and move it to the "footer"
  foreach ($js as $path => &$source) {
    //Skip if this source is in the list of excludes
    if (in_array($path, $excludes)) {
      continue;
    }
    //Change source's scope to "footer"
    $source['scope'] = 'footer';
  }
}

/**
 * Loads list of scripts to exclude from persistent variables
 * 
 * @return array
 *   Indexed array containing paths for the scripts to exclude from altering
 */
function js_to_bottom_load_excludes() {
  //Load exclusion list from persistent variables
  $exclude_list = variable_get('js_to_bottom_exclude_list', '');
  //Turn list string into array, removing any troublesome characters first
  $exclude_list = check_plain($exclude_list);
  $exclude_list = str_replace("\r", '', $exclude_list);
  $exclude_list = explode("\n", $exclude_list);

  return $exclude_list;
}

/**
 * Implements hook_js_alter().
 *
 * Calls module's function to move all scripts, unless
 * specified otherwise, to the bottom of the page.
 */
function js_to_bottom_js_alter(&$javascript) {
  //Use module's function to move JS to bottom
  js_to_bottom_move_scripts($javascript, js_to_bottom_load_excludes());
}

/**
 * Implements hook_menu().
 *
 * Registers the menu callback for the module's settings page
 */
function js_to_bottom_menu() {
  $menu_items = array();
  
  //Module administration page
  $menu_items['admin/config/development/js-to-bottom'] = array(
    'title' => 'JS-to-Bottom Settings',
    'description' => 'Configure JS-to-Bottom\'s settings, ' .
      'including which scripts to leave alone.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('js_to_bottom_settings_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'js_to_bottom.admin.inc',
  );
  
  return $menu_items;
}
