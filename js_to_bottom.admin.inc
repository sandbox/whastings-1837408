<?php

/**
 * @file
 * Defines JS-to-Bottom's administration interface
 */

/**
 * Creates the form for editing JS-to-Bottom's settings
 *
 * @param array $form
 *   The initial form array
 * @param array $form_state
 *   The initial form state
 *
 * @return array
 *   The full form array prepared by Drupal
 */
function js_to_bottom_settings_form($form, &$form_state) {
  //Text area to specify paths for scripts to leave alone
  $form['js_to_bottom_exclude_list'] = array(
    '#type' => 'textarea',
    '#title' => t('Scripts to keep where they are (e.g. in the page head)'),
    '#description' => t('By default, all JavaScript tags are moved to the ' .
      'bottom of the page. To keep a script where it is (leave it alone), list its ' .
      'path here (e.g. sites/all/modules/my_module/myscript.js). ' .
      'List one script per line. Use paths relative to Drupal\'s root for internal ' .
      'scripts. NOTE: Some JavaScript files need to load in a particular order, ' .
      'so be careful when changing how scripts load. Also, you may need to ' .
      'clear caches after changing this setting if you have page caching ' .
      ' enabled for anonymous users.'),
    '#default_value' => variable_get('js_to_bottom_exclude_list', ''),
  );
  //Return via system_settings_form()
  return system_settings_form($form);
}
